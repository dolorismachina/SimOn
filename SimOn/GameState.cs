﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace SimOn
{
    /// <summary>
    /// Holds data shared between multiple runs of the program.
    /// </summary>
    class GameState
    {
        public string SaveName { get; set; }
        public DateTime SaveTime { get; set; }
        public GameTime GameTime { get; set; }
        public Simon Sim { get; set; }

        /// <summary>
        /// Creates an empty GameState.
        /// </summary>
        public GameState()
        {
            SaveName = "";
            SaveTime = new DateTime();
            GameTime = new GameTime();
            Sim = new Simon();
        }

        /// <summary>
        /// Creates a GameState from data saved in a file.
        /// </summary>
        /// <param name="file">Path to the file containing GameState data.</param>
        public GameState(string file)
            : this()
        {
            Load(file);
        }

        /// <summary>
        /// Saves current game state to file for later retrieval.
        /// </summary>
        /// <param name="saveFile">Path to file that will hold GameState data.</param>
        public void Save(string saveFile)
        {
            if (File.Exists(saveFile))
                File.Delete(saveFile);

            FileStream fs = File.Open(saveFile, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fs);
            Console.WriteLine("SS");
            sw.Write(JsonConvert.SerializeObject(this, Formatting.Indented));

            sw.Close();
            fs.Dispose();
        }

        /// <summary>
        /// Loads data previously saved by the player.
        /// </summary>
        /// <param name="saveFile">Path to a file that stores GameState data.</param>
        public void Load(string saveFile)
        {
            FileStream fs = File.Open(saveFile, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            
            string json = sr.ReadToEnd();

            SaveName = JsonConvert.DeserializeObject<GameState>(json).SaveName;
            SaveTime = JsonConvert.DeserializeObject<GameState>(json).SaveTime;
            GameTime = JsonConvert.DeserializeObject<GameState>(json).GameTime;
            Sim = JsonConvert.DeserializeObject<GameState>(json).Sim;

            sr.Close();
            fs.Dispose();
        }
    }
}
