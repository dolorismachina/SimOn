﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML;
using SFML.Window;
using GCL;
using System.IO;
using Newtonsoft.Json;
namespace SimOn
{
    class MenuScreen : Screen
    {
        Sprite newButton = new Sprite(new Texture("new.png"));
        Sprite loadButton = new Sprite(new Texture("load.png"));
        Sprite quitButton = new Sprite(new Texture("quit.png"));

        public MenuScreen(string title, Game parent)
            : base(title, parent)
        {
            newButton.Position = new Vector2f(0, 0);
            loadButton.Position = new Vector2f(0, 75);
            quitButton.Position = new Vector2f(0, 150);

            GetSavedGameStates();
        }

        public override void Render()
        {
            Draw(newButton);
            Draw(loadButton);
            Draw(quitButton);
        }

        public override void Update(double dt)
        {
            if (IsMouseButtonPressed(Mouse.Button.Left))
            {
                // New button
                if (Mouse.GetPosition(Parent.Window).X > newButton.Position.X
                    && Mouse.GetPosition(Parent.Window).X < newButton.Position.X + newButton.Texture.Size.X
                    && Mouse.GetPosition(Parent.Window).Y > newButton.Position.Y
                    && Mouse.GetPosition(Parent.Window).Y < newButton.Position.Y + newButton.Texture.Size.Y)
                {
                    StartNewGame();
                }

                // Load button
                if (Mouse.GetPosition(Parent.Window).X > loadButton.Position.X
                    && Mouse.GetPosition(Parent.Window).X < loadButton.Position.X + loadButton.Texture.Size.X
                    && Mouse.GetPosition(Parent.Window).Y > loadButton.Position.Y
                    && Mouse.GetPosition(Parent.Window).Y < loadButton.Position.Y + loadButton.Texture.Size.Y)
                {
                    LoadGame();
                }

                // Quit button
                if (Mouse.GetPosition(Parent.Window).X > quitButton.Position.X
                    && Mouse.GetPosition(Parent.Window).X < quitButton.Position.X + quitButton.Texture.Size.X
                    && Mouse.GetPosition(Parent.Window).Y > quitButton.Position.Y
                    && Mouse.GetPosition(Parent.Window).Y < quitButton.Position.Y + quitButton.Texture.Size.Y)
                {
                    Parent.Window.Close();
                }
            }
        }

        private void StartNewGame()
        {
            PrepareDirectories();

            GameState gameState = new GameState();
            Parent.AddScreen(new SimonScreen("Game", Parent, gameState));
            Parent.ActiveScreen = "Game";
        }

        /// <summary>
        /// Checks if all required directories are created. Creates them if any of them are missing.
        /// </summary>
        private static void PrepareDirectories()
        {
            if (!Directory.Exists("Saves"))
                Directory.CreateDirectory("Saves");
        }

        private void LoadGame()
        {
            Console.WriteLine("Please type in name of the save file you wish to load: ");
            string saveFile = "Saves/" + Console.ReadLine() + ".sav";
            GameState gameState = new GameState();
            gameState.Load(saveFile);
            Parent.AddScreen(new SimonScreen("Game", Parent, gameState));
        }

        /// <summary>
        /// Searches defined path for game states saved in files.
        /// </summary>
        /// <returns>List of GameStates found in the defined path.</returns>
        private List<GameState> GetSavedGameStates()
        {
            List<GameState> gameStates = new List<GameState>();
            IEnumerable<string> saveFiles = Directory.EnumerateFiles("Saves");
            foreach (string saveFile in saveFiles)
            {
                GameState state = new GameState(saveFile);
                gameStates.Add(state);
            }

            return gameStates;
        }
    }
}
