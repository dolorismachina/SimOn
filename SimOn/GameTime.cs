﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SimOn
{
    /// <summary>
    /// Class of which sole purpose is to keep track of in-game time.
    /// </summary>
    public class GameTime
    {
        private int year;
        public int Year
        {
            get { return year; }
            set 
            {
                if (value < 0)
                    year = 0;
                else
                    year = value;
            }
        }

        private int month;
        public int Month
        {
            get { return month; }
            set
            {
                if (value < 0)
                    month = 0;
                else if (value > 12)
                {
                    month = 0;
                    Year++;
                }
                else
                    month = value;
            }
        }

        private int day;
        public int Day
        {
            get { return day; }
            set
            {
                if (value < 0)
                    day = 0;
                else if (value > 30)
                {
                    day = 0;
                    Month++;
                }
                else
                    day = value;
            }
        }

        private int hour;
        public int Hour
        {
            get { return hour; }
            set
            {
                if (value < 0)
                    hour = 0;
                else if (value > 23)
                {
                    hour = 0;
                    Day++;
                }
                else
                    hour = value;
            }
        }

        private int minute;
        public int Minute
        {
            get { return minute; }
            set
            {
                if (value < 0)
                    minute = 0;
                else if (value > 59)
                {
                    minute = 0;
                    Hour++;
                }
                else
                    minute = value;
            }
        }

        private int second;
        public int Second
        {
            get { return second; }
            set
            {
                if (value < 0)
                    second = 0;
                else
                    second = value;
            }
        }

        public void Push()
        {
            Minute++;
            Second++;
        }

        public override string ToString()
        {
            return "Year: " + Year + " | Month: " + Month + " | Day: " + Day + " | Hour: " + Hour + " | Minute: " + Minute;
        }
    }
}
