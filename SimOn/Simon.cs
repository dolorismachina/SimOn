﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SimOn
{
    public enum Stage
    {
        Infant = 10, 
        Child, 
        Teenager, 
        Adult, 
        Senior
    };

    public class Simon
    {
        private double attributeMin = 0;
        private double attributeMax =  100;

        private const double boredomPerSecond = 0.138;
        private const double hungerPerSecond = 0.0694;
        private const double thirstPerSecond = 0.2083;
        private const double tirednessPerSecond = 0.0694;
        private const double cleanlinessPerSecond = -0.03472;
        private const double agePerSecond = 1;

        private double boredom = 0;
        private double hunger = 0;
        private double thirst = 0;
        private double happiness = 100;
        private double tiredness = 0;
        private double cleanliness = 100;
        private double health = 100;
        private double toilet = 0;
        private int age = 0;

        public double Boredom
        {
            get { return boredom; }
            set 
            {
                if (value >  attributeMax)
                    boredom =  attributeMax;
                else if (value < attributeMin)
                    boredom = attributeMin;
                else
                    boredom = value;
            }
        }

        public double Hunger
        {
            get { return hunger; }
            set 
            {
                if (value >  attributeMax)
                    hunger =  attributeMax;
                else if (value < attributeMin)
                    hunger = attributeMin;
                else
                    hunger = value; 
            }
        }

        public double Thirst
        {
            get { return thirst; }
            set 
            {
                if (value > attributeMax)
                    thirst = attributeMax;
                else if (value < attributeMin)
                    thirst = attributeMin;
                else
                    thirst = value; 
            }
        }

        public double Happiness
        {
            get { return happiness; }
            set
            {
                if (value > attributeMax)
                    happiness =  attributeMax;
                else if (value < attributeMin)
                    happiness = attributeMin;
                else
                    happiness = value;
            }
        }

        public double Tiredness
        {
            get { return tiredness; }
            set 
            {
                if (value >  attributeMax)
                    tiredness =  attributeMax;
                else if (value < attributeMin)
                    tiredness = attributeMin;
                else
                    tiredness = value; 
            }
        }

        public double Cleanliness
        {
            get { return cleanliness; }
            set 
            {
                if (value >=  attributeMax)
                    cleanliness =  attributeMax;
                else if (value < attributeMin)
                    cleanliness = attributeMin;
                else
                    cleanliness = value;
            }
        }

        public double Health
        {
            get { return health; }
            set 
            {
                if (value >  attributeMax)
                    health =  attributeMax;
                else if (value < attributeMin)
                    health = attributeMin;
                else
                    health = value; 
            }
        }

        public double Toilet
        {
            get { return toilet; }
            set 
            {
                if (value > attributeMax)
                    toilet = attributeMax;
                else if (value < attributeMin)
                    toilet = attributeMin;
                else
                    toilet = value; 
            }
        }

        public int Age
        {
            get { return age; }
            set
            {
                if (value < 0)
                    age = 0;
                else
                    age = value;
            }
        }

        public void UpdateAttributes(long secondsSinceClosed)
        {
            Hunger += (double)(hungerPerSecond * secondsSinceClosed);
            Thirst += (double)(thirstPerSecond * secondsSinceClosed);
            Boredom += (double)(boredomPerSecond * secondsSinceClosed);
            Cleanliness += (double)(cleanlinessPerSecond * secondsSinceClosed);
            Tiredness += (double)(tirednessPerSecond * secondsSinceClosed);
            Age += (int)(secondsSinceClosed * agePerSecond);
        }

        public override string ToString()
        {
            // Attributes
            string attrs = "";
            attrs += "Age " + Age + "\n";
            attrs += "Hunger " + Hunger + "\n";
            attrs += "Thirst " + Thirst + "\n";
            attrs += "Cleanliness " + Cleanliness + "\n";
            attrs += "Toilet " + Toilet + "\n";
            attrs += "Health " + Health + "\n";
            attrs += "Boredom " + Boredom + "\n";
            attrs += "Tiredness " + Tiredness + "\n";
            attrs += "Happiness " + Happiness + "\n";

            return attrs;
        }
    }
}
