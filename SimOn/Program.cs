﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace SimOn
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory.CreateDirectory("Saves");

            GCL.Game g = new GCL.Game();
            
            g.AddScreen(new MenuScreen("Menu", g));
            g.ActiveScreen = "Menu";
            g.Run();
        }
    }
}
