﻿using GCL;

using System.IO;
using System;

namespace SimOn
{
    /// <summary>
    /// Main game screen where all the action takes place.
    /// </summary>
    class SimonScreen : Screen
    {
        Simon sim = new Simon();
        GameTime gt = new GameTime();
        int gameSpeed = 5;
        public GameState gameState = new GameState();

        public SimonScreen(string title, Game parent, GameState gameState)
            : base(title, parent)
        {
            this.gameState = gameState;
            Initialize();
        }

        /// <summary>
        /// Prepares the game by setting objects to a saved GameStade data.
        /// </summary>
        private void Initialize()
        {
            sim = gameState.Sim;
            gt = gameState.GameTime;
            Console.WriteLine(sim);
        }

        private DateTime timeOfLastUpdate = DateTime.Now;
        public override void Update(double dt)
        {
            if (IsKeyPressed(SFML.Window.Keyboard.Key.S))
            {
                SaveGame();
            }

            if (Utilities.GetTimeDifference(timeOfLastUpdate).Milliseconds > 1000 / gameSpeed)
            {
                gt.Push();
                sim.UpdateAttributes(gameSpeed);
                timeOfLastUpdate = DateTime.Now;
            }
        }

        private void SaveGame()
        {
            gameState.GameTime = gt;
            gameState.Sim = sim;
            gameState.Save("Saves/AutoSave.sav");
        }

        public override void Render()
        {
        }
    }
}
